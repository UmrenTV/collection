const progress = document.getElementById("progress");
const prev = document.getElementById("prev");
const next = document.getElementById("next");
const circles = document.querySelectorAll(".circle");

let currentActive = 1;

next.addEventListener("click", () => {
  if (currentActive < circles.length) {
    currentActive++;
    updateProgress();
    prev.disabled = false;
    if (currentActive === circles.length) {
      next.disabled = true;
    }

    circles[currentActive - 1].classList.add("active");
  }
});
prev.addEventListener("click", () => {
  if (currentActive > 1) {
    currentActive--;
    updateProgress();
    next.disabled = false;
  }
  if (currentActive < 2) {
    prev.disabled = true;
  }

  circles[currentActive].classList.remove("active");
});

function updateProgress() {
  const progressInPercent = 100 / (circles.length - 1);
  let currentProgressinPercent =
    parseInt(progressInPercent, 10) * (currentActive - 1);
  progress.style.width = currentProgressinPercent.toString() + "%";
  circles[currentActive - 1].classList.add("active");
}
